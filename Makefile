.PHONY: all
all: static/bulma.css static/logo.svg

static/bulma.css: bulma/bulma.sass
	sass --sourcemap=none --style compressed $< $@

static/logo.svg: tikz/logo.tex
	xelatex -output-directory=tikz -shell-escape $<
	pdf2svg tikz/logo.pdf $@

.PHONY: clean
clean:
	rm -f static/bulma.css
	rm -f static/logo.svg
