from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name='pages/index.html'), name='index'),
    path('x86-64/', TemplateView.as_view(template_name='pages/x86_64.html'), name='x86-64'),
    path('ascii/', TemplateView.as_view(template_name='pages/ascii.html'), name='ascii'),
    path('elf/', TemplateView.as_view(template_name='pages/elf.html'), name='elf'),
    path('linux/', TemplateView.as_view(template_name='pages/linux.html'), name='linux'),
]
